package week4.Day1;

import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnFrame {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
	driver.manage().window().maximize();
	driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
	//control enters inside frame
	driver.switchTo().frame("iframeResult");
	driver.findElementByXPath("//button[text()='Try it']").click();
	//handle alert
	Alert promptAlert = driver.switchTo().alert();
	promptAlert.sendKeys("TestLeaf");
	promptAlert.accept();
	System.out.println(driver.findElementByTagName("p").getText());
      
	}

}
