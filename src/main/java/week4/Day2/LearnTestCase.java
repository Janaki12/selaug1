package week4.Day2;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnTestCase {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("http://leaftaps.com/opentaps/");
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByXPath("//a[text()='Leads']").click();
		driver.findElementByXPath("//a[text()='Merge Leads']").click();
		driver.findElementByXPath("//table[@id='widget_ComboBox_partyIdFrom']/following-sibling::a").click();
		
		Set<String> windowHandles = driver.getWindowHandles();
		List<String> lst=new ArrayList<>();
		lst.addAll(windowHandles);
		driver.findElementByXPath("(//input[@id='ComboBox_partyIdFrom']//following::img)[1]").click();
		windowHandles = driver.getWindowHandles();
		lst=new ArrayList<>();
		lst.addAll(windowHandles);
		driver.switchTo().window(lst.get(1));
		driver.findElementByXPath("//input[@name='id']").sendKeys("10");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(3000);
		WebElement Lead1 = driver.findElementByXPath("((//div[text()='Lead ID']//following::tr)[1]/td)[1]");
		String text=Lead1.getText();
		Thread.sleep(3000);

		Lead1.click();
		System.out.println(text);
		driver.switchTo().window(lst.get(0));
		driver.findElementByXPath("(//input[@id='ComboBox_partyIdFrom']//following::img)[2]").click();
		driver.switchTo().window(lst.get(1));
		driver.findElementByXPath("//input[@name='id']").sendKeys("10215");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(3000);
		driver.findElementByXPath("((//div[text()='Lead ID']//following::tr)[1]/td)[1]").click();;
		driver.switchTo().window(lst.get(0));
		driver.findElementByLinkText("Merge").click();
		driver.switchTo().alert().accept();
		String line1 = driver.findElementByXPath("//div[@class='messages']/div").getText();
		String line2 = driver.findElementByXPath("(//div[@class='messages']/div//following::li)[1]").getText();
		String line3 = driver.findElementByXPath("(//div[@class='messages']/div//following::li)[1]").getText();
		System.out.println(line1+"\n"+line2+"\n"+line3);
		driver.findElementByLinkText("Find Leads").click();
		Thread.sleep(3000);
		driver.findElementByXPath("//input[@name='id']").sendKeys("10");
		driver.findElementByXPath("//button[text()='Find Leads']").click();

		driver.close();
			}
		}

		

		
	


