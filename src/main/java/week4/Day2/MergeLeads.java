package week4.Day2;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class MergeLeads {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		driver.get("http://leaftaps.com/opentaps/");
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Merge Leads").click();
		driver.findElementByXPath("//input[@id='partyIdFrom']/following-sibling::a").click();
		//control goes to window using set
		Set<String> mergeLeads=driver.getWindowHandles();
		//using list get which window control should work
		List<String>mergeLeadList=new ArrayList<String>();
		//add all data to List from Set
		mergeLeadList.addAll(mergeLeads);
		//get index of window to use(Move to new window)
		driver.switchTo().window(mergeLeadList.get(1));
		driver.manage().window().maximize();
		driver.findElementByName("id").sendKeys("11");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(7000);
		driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']//a").click();
		//switch to primary window
		//to go to To Lead		
		driver.switchTo().window(mergeLeadList.get(0));
		
		driver.findElementByXPath("//input[@id='partyIdFrom']/following-sibling::a").click();

		mergeLeads=driver.getWindowHandles();
		mergeLeadList=new ArrayList<String>();
		mergeLeadList.addAll(mergeLeads);
		driver.switchTo().window(mergeLeadList.get(1));
		
		driver.findElementByName("id").sendKeys("10");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(7000);
		driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']//a").click();
		driver.switchTo().window(mergeLeadList.get(0));

		driver.findElementByLinkText("Merge").click();
		
		//to handle alert
		driver.switchTo().alert().accept();
		
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("//input[@name='id']").sendKeys("2000");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(7000);
		if((driver.findElementByXPath("//div[@class='x-paging-info']").getText()).contains("No records to display"))
		{
			System.out.println("'No records to display' is displayed. Hence Merge lead is sucessfully completed");
		}
		else
		{
			System.out.println("Merge lead is not sucessfully completed");
		}
		driver.close();
		
		
		
		
	}

}
