package testcase;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class DulpicateLead extends ProjectMethods {
	@BeforeTest
	public void setData()
	{
		testCaseName="DulpicateLead";
		testCaseDesc="Find Dulpicate";
		category="smoke";
		author="Janaki";
	}
	@Test
	public void duplicate() throws InterruptedException {
		click(locateElement("linktext","Leads"));
		click(locateElement("linktext","Find Leads"));
		click(locateElement("xpath","//span[text()='Email']"));
		type(locateElement("name","emailAddress"), "rjhfja@gmail.com");
		click(locateElement("xpath","//button[text()='Find Leads']"));
		Thread.sleep(7000);
		String leadName = getText(locateElement("xpath","//td[@class='x-grid3-col x-grid3-cell x-grid3-td-firstName ']//a"));
		click(locateElement("xpath","//td[@class='x-grid3-col x-grid3-cell x-grid3-td-firstName ']//a"));
		click(locateElement("linktext","Duplicate Lead"));
		verifyTitle("Duplicate Lead");
		click(locateElement("xpath","//input[@type='submit']"));
		verifyExactText(locateElement("id","viewLead_firstName_sp"), leadName);
		closeBrowser();	
	}

}
