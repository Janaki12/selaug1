package testcase;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class CreateLead extends ProjectMethods{
	@BeforeTest
	public void setData()
	{
		testCaseName="CreateLead";
		testCaseDesc="Create a new Lead";
		category="smoke";
		author="sethu";
	}
	
	@Test
		public void createLead() {
			/*startApp("chrome", "http://leaftaps.com/opentaps");
			WebElement eleUserName = locateElement("id", "username");
			type(eleUserName, "DemoSalesManager");
			WebElement elePassword = locateElement("id","password");
			type(elePassword, "crmsfa");
			WebElement eleLogin = locateElement("class", "decorativeSubmit");
			click(eleLogin);
			WebElement eleLogout = locateElement("class", "decorativeSubmit");
			click(eleLogout);
			WebElement eleLink = locateElement("linkText", "CRM/SFA");
			click(eleLink);*/
			WebElement elebutton = locateElement("linkText", "Leads");
			click(elebutton);
			WebElement elebutton1 = locateElement("linkText", "Create Lead");
			click(elebutton1);
			WebElement cName = locateElement("createLeadForm_companyName");
			type(cName, "Polaris");
			WebElement fName = locateElement("createLeadForm_firstName");
			type(fName, "Sushma");
			WebElement sName = locateElement("createLeadForm_lastName");
			type(sName, "C");
			WebElement eleDropdown = locateElement("createLeadForm_dataSourceId");
			selectDropDownUsingText(eleDropdown, "Direct Mail");
			WebElement eleDropdown1 = locateElement("createLeadForm_marketingCampaignId");
			selectDropDownUsingIndex(eleDropdown1, 2);
			WebElement cLead = locateElement("class", "smallSubmit");
			click(cLead);	
	}
}
