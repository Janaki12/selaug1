package group;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class Duplicate extends ProjectMethods {
	@BeforeTest(groups = {"common"})
	public void setValuesDuplicateLead()
	{
		testCaseName = "Duplicate";
		testCaseDesc = "Duplicate Lead";
		author ="Sumathy";
		category = "Smoke";
		inputFileName="Duplicate";
	}
	/*@DataProvider(name="emailForDupl")
	public Object[] fetchData1()
	{
		Object data[]= new Object[2];
		data[0] ="preethimanik@gmail.com";
		data[1] ="binduDevi@gmail.com";
		return data;
		
	}*/
	@Test(groups = {"regression"}, dataProvider="fetchData")
	public void DuplicateLead(String leadEmail) throws InterruptedException
	{
		click(locateElement("linktext","Leads"));
		click(locateElement("linktext","Find Leads"));
		click(locateElement("xpath","//span[text()='Email']"));
		type(locateElement("name","emailAddress"), leadEmail);
		click(locateElement("xpath","//button[text()='Find Leads']"));
		Thread.sleep(7000);
		String leadName = getText(locateElement("xpath","//td[@class='x-grid3-col x-grid3-cell x-grid3-td-firstName ']//a"));
		click(locateElement("xpath","//td[@class='x-grid3-col x-grid3-cell x-grid3-td-firstName ']//a"));
		click(locateElement("linktext","Duplicate Lead"));
		verifyTitle("Duplicate Lead");
		click(locateElement("xpath","//input[@type='submit']"));
		verifyExactText(locateElement("id","viewLead_firstName_sp"), leadName);
		closeBrowser();	
		
		
	}
}

