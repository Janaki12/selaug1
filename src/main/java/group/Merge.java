package group;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class Merge extends ProjectMethods{
	@BeforeTest (groups = {"common"})
	public void setValues()
	{
		testCaseName =  "Merge";
		testCaseDesc = "Merge Lead";
		category = "Smoke";
		author = "Sumathy";
		inputFileName="Merge";
	}
	/*@DataProvider(name = "LeadIdsForMergeLead", indices= {0})
	public Object[][] fetchData()
	{
		Object data[][]= new Object[2][2];
		data[0][0]="10";
		data[0][1]="11";
		
		data[1][0]="100";
		data[1][1]="110";
		return data;
	}*/
	@Test (groups = {"regression"}, dataProvider="fetchData")
public void MergeLead(String fromLeadId, String toLeadId) throws InterruptedException
{
	click(locateElement("linktext","Leads"));
	click(locateElement("linktext","Merge Leads"));
	click(locateElement("xpath","//input[@id='partyIdFrom']/following-sibling::a/img"));
	Thread.sleep(7000);
	switchToWindow(1);
	type(locateElement("name", "id"), fromLeadId);
	click(locateElement("xpath","//button[text()='Find Leads']"));
	Thread.sleep(7000);
	String fromId = getText(locateElement("xpath","//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']//a"));
	clickWithNoSnap(locateElement("xpath","//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']//a"));
	
	switchToWindow(0);
	click(locateElement("xpath","//input[@id='partyIdTo']/following-sibling::a/img"));
	Thread.sleep(7000);
	switchToWindow(1);
	type(locateElement("name", "id"), toLeadId);
	click(locateElement("xpath","//button[text()='Find Leads']"));
	Thread.sleep(7000);
	clickWithNoSnap(locateElement("xpath","//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']//a"));
	switchToWindow(0);
	clickWithNoSnap(locateElement("linktext","Merge"));
	acceptAlert();
	click(locateElement("linktext","Find Leads"));
	type(locateElement("name", "id"), fromId);
	click(locateElement("xpath","//button[text()='Find Leads']"));
	Thread.sleep(7000);
	verifyExactText(locateElement("xpath","//div[@class='x-paging-info']"), "No records to display");
	closeBrowser();	
}


}
