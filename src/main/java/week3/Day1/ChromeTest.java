package week3.Day1;



import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class ChromeTest {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		
		try {
			
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.get("http://leaftaps.com/opentaps/");
			driver.findElementById("username1").sendKeys("DemoSalesManager");
			driver.findElementById("password").sendKeys("crmsfa");
			driver.findElementByClassName("decorativeSubmit").click();
			driver.findElementByLinkText("CRM/SFA").click();
			driver.findElementByLinkText("Create Lead").click();
			driver.findElementById("createLeadForm_companyName").sendKeys("TCS");
			driver.findElementById("createLeadForm_firstName").sendKeys("Virat");
			driver.findElementById("createLeadForm_lastName").sendKeys("Kohli");
			WebElement sourceDropDown = driver.findElementById("createLeadForm_dataSourceId");
			Select sourceBox = new Select(sourceDropDown);
			sourceBox.selectByVisibleText("Direct Mail");
			WebElement marketCampaign = driver.findElementById("createLeadForm_marketingCampaignId");
			Select marketCampaignBox =  new Select(marketCampaign);

			marketCampaignBox.selectByIndex(marketCampaignBox.getOptions().size()-2);
			
			//int size = marketCampaignBox.getOptions().size();
			//marketCampaignBox.selectByIndex(size -2);
			
			/*List<WebElement> marketCmpaignList = marketCampaignBox.getOptions();
			int size = marketCmpaignList.size();
			marketCampaignBox.selectByIndex(size -2);
			*/
			
		
			driver.findElementByName("submitButton").click();
		} catch (NoSuchElementException e) 
		{
			System.out.println("Exception here");
			//e.printStackTrace();
		}
		
	
		finally
		{
			driver.close();
		}

	}

}
