package utils;

//import java.io.IOException;

//import org.testng.annotations.DataProvider;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

//import week6.Day2.TestExcel;

public class Reporter {
	public static ExtentReports extent;
	public static ExtentTest test;
	public static String testCaseName,testCaseDesc,category,author,inputFileName;
	public void beginResult() {
		ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/TestNGAnnotations.html");
	    html.setAppendExisting(false);
		extent = new ExtentReports();
		extent.attachReporter(html);
	}	
	
	public void startTestCase() {
		test = extent.createTest(testCaseName, testCaseDesc);
		test.assignCategory(category);
		test.assignAuthor(author);
	}
	
	public void reportStep(String desc,String status) {
		if(status.equalsIgnoreCase("Pass")) {
			test.pass(desc);			
		} else if(status.equalsIgnoreCase("Fail")) {
			test.fail(desc);
			//throw new RuntimeException("Exception occured element not find");
		} else if(status.equalsIgnoreCase("Warning")) {
			test.warning(desc);			
		}	
		else if(status.equalsIgnoreCase("Info")) {
			test.info(desc);	
			
		}
	}	
	/*@DataProvider(name ="positive")
	public Object[][] fetchData() throws IOException
	{
		return TestExcel.getExcelData(excelFileName);
	}*/

	
	public void endResult() {
		extent.flush();
	}	
}















