package HomeWork;

import java.util.Scanner;

public class LearnEvenNumberBetween20To200 {

	public static void main(String[] args) {
		// even number between 20 to 200
		int n,i;
        
        Scanner X = new Scanner(System.in);
         
        System.out.print("Enter value n : ");
        n = X.nextInt();
         
        for(i=20; i<=n; i++)
        {
            if(i%2==0)
                System.out.print(i+" ");
        }   
        System.out.println();
         
      X.close();
	}

}
