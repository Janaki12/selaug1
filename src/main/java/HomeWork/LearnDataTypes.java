package HomeWork;

public class LearnDataTypes {

	public static void main(String[] args) {
			//Primitive Data types 8
			//Default valueof boolean:false, memory size:1 bit,value range:true or false
		 boolean a=true;
		//Default value of byte:0, memory size:8 bits,value range: -128 to +127
		 byte b=4;
		//Default value of char:\u0000, memory size:16-bits Unicode character,value range: �\u0000� (or 0) to �\uffff� 65535
		 char c='j';
		//Default value of short:0, memory size:16-bits,value range: -32,768 to 32,767 (inclusive)
		 short s=4;
		//Default value of interger:0, memory size:32 bits,value range: -2^31 to 2^31-1: for java se 8  [0, 2^32-1]
		 int i=100;
		//Default value og long:0, memory size:64bits,value range:2^63 to 2^63-1. java se 8:2^64-1
		 long l=-2;
		//Default value of float:0.0, memory size:32bits,value range:�3.40282347E+38F (6-7 significant decimal digits)
		 float f=1.8f;
		//Default value of double:0.0, memory size:64 bits,value range:�1.79769313486231570E+308 (15 significant decimal digits)
		 double d=0.25d;
		 //String or other object default valure :null
		 
		 System.out.println("boolean: " +a); 
		 System.out.println("byte: " +b);
		 System.out.println("char: " +c); 
		 System.out.println("short: " +s);
		 System.out.println("integer: " +i); 
		 System.out.println("long: " +l);
		 System.out.println("float: " +f); 
		 System.out.println("double: " +d); 

		}
		}	
	
