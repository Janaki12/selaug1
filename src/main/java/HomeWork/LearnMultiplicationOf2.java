package HomeWork;

//import java.util.Scanner;

public class LearnMultiplicationOf2 {
	public static void main(String[] args) {
		// Multiplication of Two
		 int n=2, c;
	      System.out.println("Enter an integer to print it's multiplication table");
	      //can use this to get table number during run time
	      /*Scanner scan = new Scanner(System.in);
	      n = scan.nextInt();*/
	      System.out.println("Multiplication table of " + n + " is : ");
	 
	      for (c = 1; c <= 20; c++)
	         System.out.println(n + "*" + c + " = " + (c*n));
	      //scan.close();
		

	}

}
