package HomeWork;

import java.util.Scanner;

public class LearnLogicalOperation {

	public static void main(String[] args) {
		// Logical Opeartors
String x = "Sher";
String y = "Locked";

Scanner s = new Scanner(System.in);
System.out.print("Enter username:");
String uuid = s.next();
System.out.print("Enter password:");
String upwd = s.next();

//&& , Logical AND : returns true when both conditions are true.
//|| , Logical OR : returns true if at least one condition is true.
// Check if user-name and password match or not.
if ((uuid.equals(x) && upwd.equals(y)) || 
        (uuid.equals(y) && upwd.equals(x))) {
    System.out.println("Welcome user.");
} else {
    System.out.println("Wrong uid or password");
   
}
{
int a = 0x0005;
int b = 0x0007;
//Bitwise Operators
// bitwise and
// 0101 & 0111=0101
System.out.println("a&b = " + (a & b));

// bitwise and
// 0101 | 0111=0111
System.out.println("a|b = " + (a | b));

// bitwise xor
// 0101 ^ 0111=0010
System.out.println("a^b = " + (a ^ b));

// bitwise and
// ~0101=1010
System.out.println("~a = " + ~a);

// can also be combined with
// assignment operator to provide shorthand
// assignment
// a=a&b
a &= b;
System.out.println("a= " + a);
}
//
int a = 20, b = 10, c = 0, d = 20, e = 40;
boolean condition = true;

// pre-increment operator
// a = a+1 and then c = a;
c = ++a;
System.out.println("Value of c (++a) = " + c);

// post increment operator
// c=b then b=b+1
c = b++;
System.out.println("Value of c (b++) = " + c);

// pre-decrement operator
// d=d-1 then c=d
c = --d;
System.out.println("Value of c (--d) = " + c);

// post-decrement operator
// c=e then e=e-1
c = --e;
System.out.println("Value of c (--e) = " + c);

// Logical not operator
System.out.println("Value of !condition =" + !condition);

s.close();
	}

}
