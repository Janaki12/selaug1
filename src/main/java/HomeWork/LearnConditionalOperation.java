package HomeWork;

import java.util.Scanner;

public class LearnConditionalOperation {
	public static void main(String[] args) {
		// To get input from user
		System.out.println("Enter the numbers:" );
		Scanner scan=new Scanner(System.in);
		int n1= scan.nextInt();
		int n2= scan.nextInt();
		
		//Conditional or Relational Operators
		//== , Equal to : returns true of left hand side is equal to right hand side.
		System.out.println("n1 is equal to n2:"+(n1==n2));
		//!= , Not Equal to : returns true of left hand side is not equal to right hand side.
		System.out.println("n1 is not equal to n2:"+(n1!=n2));
		//< , less than : returns true of left hand side is less than right hand side.
		System.out.println("n1 is less than n2:"+(n1<n2));
		//<= , less than or equal to : returns true of left hand side is less than or equal to right hand side.
		System.out.println("n1 is less or equal to n2:"+(n1<=n2));
		//> , Greater than : returns true of left hand side is greater than right hand side.
		System.out.println("n1 is greater than n2:"+(n1>n2));
		//>= , Greater than or equal to: returns true of left hand side is greater than or equal to right hand side.
		System.out.println("n1 is greater than or equal to n2:"+(n1>=n2));
		scan.close();
		
	}


}
