package HomeWork;

import java.util.Scanner;

public class LearnAssignmentOperation {

	public static void main(String[] args) {
		// To get input from user
		System.out.println("Enter the numbers:" );
		Scanner scan=new Scanner(System.in);
		int n1= scan.nextInt();
		int n2= scan.nextInt();
		
		//+=, for adding left operand with right operand and then assigning it to variable on the left.
		// here n1=4 and n2=2 if I take
		 System.out.println("number1 adding number2:\n"+(n1+=n2));
		 //now n1=(n1+n2) so n1=6 and n2=2
	    //-=, for subtracting left operand with right operand and then assigning it to variable on the left.
	    System.out.println("number1 minus number2:\n"+(n1-=n2));
	    //now  n1=4 and n2=2
		//*=, for multiplying left operand with right operand and then assigning it to variable on the left.
	    System.out.println("number1 multiply number2:\n"+(n1*=n2)); 
	    //now n1=8 and n2=2
	    //=, for dividing left operand with right operand and then assigning it to variable on the left
	    System.out.println("number1 divide number2:\n"+(n1/=n2));
	    //now n1=4 and n2=2
	    //%=, for assigning modulo of left operand with right operand and then assigning it to variable on the left.
	    System.out.println("number1 modulus number2:\n"+(n1%=n2));
	    //now n1=0 and n2=2
	    //n1 value and n2 value
	    System.out.println("n1 is:"+n1 +"\n and n2 is:" +n2);
	    scan.close();
	}
	

}
