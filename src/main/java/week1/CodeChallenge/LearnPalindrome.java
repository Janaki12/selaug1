/*step1:read input from user
step2:Assign those input to another variable to make reverse of number
step3:use multiplication and modulus and addition to reverse number
setp4:use divider to get rn value.compare n and rn1 
Step5:if condition pass then enter number will be Palindrome and ifcondition fails
 will not be Palindrome
************************************************************************ */
package week1.CodeChallenge;

import java.util.Scanner;

public class LearnPalindrome {

		public static void main(String[] args) {
			//Get Input from user
			Scanner s=new Scanner(System.in);
			int n=s.nextInt();
			//assign the input value to other variable 
			int rn=n;
			//make the input value to reverse
			int rn1=0;
			while(rn>0) {
				rn1=(rn1*10)+(rn%10);
				rn=rn/10;
			}
			if(n==rn1)
			{
				System.out.println("The Number Is Palindrome");
			}
			else
			{
				System.out.println("The Number Is Not A Palindrome");	
			}
			s.close();
		}	
			
		}

	
