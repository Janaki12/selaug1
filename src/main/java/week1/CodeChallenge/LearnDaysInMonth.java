/*
 Pseudo code:
 Step1: Read month and year 
 Step2:verify that which number matches using switch case condition and print the statement.
 step3:in case2: we have two conditions which is either leap year or not.if number either divisble by 400 or 4 but 
 should not by 100 then its leap year
 step4:print console data. 
 ..................................................................*/
package week1.CodeChallenge;

import java.util.Scanner;

public class LearnDaysInMonth {

	static String monthname;
	static int number_Of_DaysInMonth; 

	public static void main(String[] args) {
		// Enter the month as integer
		
		Scanner s=new Scanner(System.in);
		System.out.print("Enter the month in number: ");
        int month = s.nextInt();

        System.out.print("Enter the year: ");
        int year=s.nextInt();
        
        switch (month) {
            case 1:
            	monthname="January";
            	number_Of_DaysInMonth=31;
            	break;
            case 2:
            	monthname="February";
            	if((year%400==0)||(year%4==0)&&(year%100!=0))
            	{
        	   number_Of_DaysInMonth=29;
           }
else
{
	number_Of_DaysInMonth=28;
}
            	break;
            case 3:
            	monthname="March";
            	number_Of_DaysInMonth=31;
            	break;
            case 4:
            	monthname="April";
            	number_Of_DaysInMonth=30;
            	break;
            case 5:
            	monthname="May";
            	number_Of_DaysInMonth=31;
            	break;
            case 6:
            	monthname="June";
            	number_Of_DaysInMonth=30;
            	break;
            case 7:
            	monthname="July";
            	number_Of_DaysInMonth=31;
            	break;
            case 8:
            	monthname="August";
            	number_Of_DaysInMonth=31;
            	break;
            case 9:
            	monthname="September";
            	number_Of_DaysInMonth=30;
            	break;
            case 10:
            	monthname="October";
            	number_Of_DaysInMonth=31;
            	break;
            case 11:
            	monthname="November";
            	number_Of_DaysInMonth=30;
            	break;
            case 12:
            	monthname="December";
            	number_Of_DaysInMonth=31;
            	break;
	}
    	System.out.println("The "+ month +" month of the Year is:"+monthname);
        System.out.print(monthname + " " + year + " has " + "Number of days: " + number_Of_DaysInMonth );
         s.close();
	}
}


