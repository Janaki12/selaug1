package week1.CodeChallenge;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class LearnTestLeaf {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");	
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("http://leafground.com/pages/Dropdown.html");
		
		//Find Dropdown using index
		WebElement src = driver.findElementById("dropdown1");
		Select dropDown = new Select(src);
		List<WebElement> options = dropDown.getOptions();
		dropDown.selectByIndex(options.size()-2);
		
		//Find dropdown using text
		WebElement src1 = driver.findElementByName("dropdown2");
		Select dropDown1 = new Select(src1);
		dropDown1.selectByVisibleText("Appium");
		
	//find by value
		WebElement src2 = driver.findElementById("dropdown3");
		Select dropDown3 = new Select(src2);
		dropDown3.selectByValue("1");
		
	}

}
