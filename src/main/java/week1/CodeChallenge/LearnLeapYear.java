package week1.CodeChallenge;

import java.util.Scanner;

public class LearnLeapYear {

	public static void main(String[] args) {
		Scanner s=new Scanner(System.in);
		System.out.println("Enter the year: ");
		int year=s.nextInt();
		if((year % 4 == 0)||(year % 400 == 0)&&(year % 100 != 0))
		{
				System.out.println("The Year is leap year");
			
		}
			else {
				System.out.println("The Year is not leap year");
			}	

	}

}
