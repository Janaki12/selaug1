/*Pseudo code:
 step1:Read input from user
 step2:assign input to other varaibale
 step3:using Mod,Mul and ADD will get rn value
 step4: will get the reverse value
 ..........................................................*/
package week1.CodeChallenge;

import java.util.Scanner;

public class LearnReverse {

	public static void main(String[] args) {
		//Get Input from user
		Scanner s=new Scanner(System.in);
		System.out.println("Enter the input:");
		int n=s.nextInt();
		//assign the input value to other variable 
		int rn=n;
		//make the input value to reverse
		int rn1=0;
		while(rn>0) {
			rn1=(rn1*10)+(rn%10);
			rn=rn/10;
		}
		System.out.println("The Reverse for given number is: "+rn1);
		s.close();

	}

}
