package week1.CodeChallenge;
import java.util.Scanner;

public class LearnSecondSmallestNum {
	   public static void main(String[] args) {
			// find second smallest number
			//To read 3 interger input from user
	     Scanner s=new Scanner(System.in);
	     int n1=s.nextInt();
	     int n2=s.nextInt();
	     int n3=s.nextInt();
	     //compare number1 with number2 and number3 
	     if(((n1>n2)&&(n1<n3))&&(n2<n3))
	    	  {
	    		 System.out.println("second smallest number is:"+n1);
	    	 }
	   //compare number2 with number1 and number3 
	     else if (((n2>n1)&&(n2<n3))&&(n1<n3))
	    	  {
	    		 System.out.println("second smallest number is:"+n2);
	    	 }
	   //number 3 will be second smallest number
	     else 
	     {
	    	 System.out.println("second smallest number is:"+n3);
	     }
	     s.close();
		}
		
	}

