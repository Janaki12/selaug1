	package week1.CodeChallenge;
	
	import java.util.Scanner;
	
	public class FloydTriangle {
	
		public static void main(String[] args) {
		Scanner scan=new Scanner(System.in);
		System.out.print("Enter the number of row: ");
		int row=scan.nextInt();
		int num=1;
		for (int i=1;i<=row;i++) {
			for(int j=1;j<=i;j++) {
				System.out.print(num+" ");
				num++;
			}
			System.out.println();
		}
	
		}
	
	}
