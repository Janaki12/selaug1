package week1.CodeChallenge;

import java.util.Scanner;

public class LearnSumOf3Or4 {

	public static void main(String[] args) {
		Scanner s=new Scanner(System.in);
		System.out.println("Enter the number: ");
		int n=s.nextInt();
		int sum = 0;
		System.out.println("The multiples of 3 or 5 is: ");
		for (int i = 0; i < n; i++) {
		  if (i % 3 == 0 || i % 5 == 0) {
		    sum += i;
		    System.out.println(""+i+",");
		  }
		}
		System.out.println("The sum of multiples of 3 or 5 is: "+sum);

	}

}
