package week1.CodeChallenge;

import java.util.Scanner;

public class LearnPercentageLettersInString {

	public static void main(String[] args) {
		//Scanner scan=new Scanner(System.in);
		//String sentence =scan.next();
		//System.out.println("Enter the Sentence");
		String sentence="Tiger Runs @ The Speed Of 100 km/hour";
		char charArray[]=sentence.toCharArray();
		float upperCase=0,lowerCase=0,digits=0,otherChar=0;
	      float totalChars = sentence.length();
	      for(float i=0; i<sentence.length(); i++) {
	    	  if (Character.isUpperCase(charArray[(int) i])) {
	              upperCase++;
	           } else if(Character.isLowerCase(charArray[(int) i])) {
	              lowerCase++;
	           } else if(Character.isDigit(charArray[(int) i])){
	        	   digits++;
	           } else {
	              otherChar++;
	           }
	        }
	        System.out.println("Total length of the string is : "+totalChars);
	        System.out.println("Number of Uppercase letters is : "+upperCase);
	        System.out.println("Percentage of upper case letters: "+((upperCase*100)/totalChars)+"%");
	        System.out.println("Lower case :"+lowerCase);
	        System.out.println("Percentage of lower case letters:"+((lowerCase*100)/totalChars)+"%");
	        System.out.println("Digit :"+digits);
	        System.out.println("Percentage of digits :"+((digits*100)/totalChars)+"%");
	        System.out.println("Others :"+otherChar);
	        System.out.println("Percentage of other characters :"+((otherChar*100)/totalChars)+"%");
	     }
	      }

	
