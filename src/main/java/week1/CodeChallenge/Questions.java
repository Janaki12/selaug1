/*1)why main method is static?
This is because main() method is entry point of a program,it should be called before creating object of class.Main advantage of this is it saves memory.if we run java program without declaring main() as static, we can only compile but cant run the program.
main() is called by JVM before any objects are created when we declared as Static.
Can we excute a program without main method?
No we cant excute without main method unless we call that class in main method,static block wont produce output.
....................................................................................
2)static method:
Static members belong to the class instead of a specific instance, 
this means if you make a member static, you can access it without object.
we can call static  method without any object because when we make a member static it becomes class level.

static block:
Static block will initialize the static variables.block gets executed when the class is loaded in the memory. 
A class can have multiple Static blocks, which will execute in the same sequence in which they have been written into the
 program.

static variable:
static variable is created only onceand shared among all the instances of the class. 
Memory allocation for these variables  happens only once when the class is loaded in the memory.
it is also called as class variable.	

*/
