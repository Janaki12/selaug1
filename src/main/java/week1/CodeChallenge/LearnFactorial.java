/*step1: Read input from user
step2:Intialize variable start as 1.
step3:verify conditions and muliples the value.
step4:once conditon fails will print value of Factorial.
*********************************************************************** */
package week1.CodeChallenge;

import java.util.Scanner;

public class LearnFactorial {

	public static void main(String[] args) {
		//Get Input from user
		System.out.println("Enter the number: ");
		Scanner s=new Scanner(System.in);
		long n=s.nextLong();
		long fact=1;
		for(long i=1;i<=n;i++)
		{
			fact*=i;
		}
		System.out.println("Factorial of Given number is:"+fact);
		s.close();
	}

}
