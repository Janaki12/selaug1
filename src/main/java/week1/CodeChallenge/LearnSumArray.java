package week1.CodeChallenge;

import java.util.Scanner;

public class LearnSumArray {

	public static void main(String[] args) throws InterruptedException {
		// First read the size of array from user 
		int sum=0;
		Scanner scr = new Scanner(System.in);
		System.out.println("Enter the size of array:"); 
		//enter all the elements of that array
		int  n=scr.nextInt();
		int a[]=new int[n];
		Thread.sleep(2000);
		System.out.println("Enter "+n +" Array Values:");
		//calculate the sum of elements of the array with for loop
		for(int i=0; i<n; i++) {
			a[i]=scr.nextInt();
        //Sum of array caluclated
			sum+=a[i];
		}
        System.out.println("sum of "+n+" numbers is = "+sum);   
        scr.close();
	}

}
