package week1.CodeChallenge;

import java.util.Scanner;

public class LearnArmstrongNumber {

	public static void main(String[] args) {
	
		Scanner scan = new Scanner(System.in);
		      System.out.println("Enter the number to check Armstrong or not: ");      
		     int num  = scan.nextInt();

		        int  n, temp, total = 0;

		        n = num;
		        
		        while (n != 0)
		        {
		            temp = n % 10;
		            total = total + temp*temp*temp;
		            n /= 10;
		        }

		        if(total == num)
		            System.out.println(num + " is an Armstrong number");
		        else
		            System.out.println(num + " is not an Armstrong number");
		    }
}
	
