package week1.CodeChallenge;

import java.util.Scanner;

public class LearnStringPasswordValid {
	public static boolean isValidPassword(String s)

	 {
		/*Password rules:
			A password must have at least ten characters.
			A password consists of only letters and digits.
			A password must contain at least two digits and two letters.*/
	
		    if (s == null)
		
		        return false;
		
		    if (s.length() < 8)
		
		        return false;
		
		    int counter = 0;
		
		    for (int i = 0; i < s.length(); i++)
		
		    {
		
		        if (Character.isDigit(s.charAt(i)))
		
		        {
		
		            counter++;
		
		        }
		
		    }
		
		        if (counter >= 2)
		
		            return true;
		
		     
		
		    return false;

		
	}


public static void main(String[] args) {
	LearnStringPasswordValid s =new LearnStringPasswordValid();
	LearnStringPasswordValid.isValidPassword("Appe123");
	System.out.println(s);
}
}