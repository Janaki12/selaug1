/*Pseudo code:
 Step1:Read input from user
 Step2:Verify n1 with equals to n.
 Step3: If condition true continue loop and print the value of 
 Fibonacci Series
 ......................................................*/
 package week1.CodeChallenge;
 

import java.util.Scanner;

public class LearnFibonacci {

	public static void main(String[] args) {
		int n1=0,n2=1;
		//Get Number from user
		System.out.println("Enter the number: ");
		Scanner S=new Scanner(System.in);
		int n=S.nextInt();   
		//compare with n1 equals to n
		while(n1<=n)
		{
	    //Find Fibonacci Series
		System.out.print(+n1 +" ");		
	    int fib=n1+n2;
		n1=n2;
		n2=fib;
		}
		
		S.close();
	}

}
