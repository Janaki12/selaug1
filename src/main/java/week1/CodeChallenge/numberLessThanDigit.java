package week1.CodeChallenge;

public class numberLessThanDigit {

	public static int greatestNumber(int number,int digit){
		
		//convert from digit to char
		char c = Integer.toString(digit).charAt(0);
		
		for (int i = number; i > 0; --i)
		{
			if(Integer.toString(i).indexOf(c) == -1) {
				return i;
			}
		}
		return -1;
		 
	}


public static void main(String[] args) {
	System.out.println("The Greatest number is: ");
	 System.out.println(greatestNumber(156, 5));
	 
}
}