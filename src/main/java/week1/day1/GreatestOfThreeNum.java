/*Pseudo Code:
 Step1: Read the input from user
 Step2:Verify first number with second and third number to find out Larger and display if condition satisfy
 Step3:Verify second number with first and third number to find out Larger and display if condition satisfy
 Step4:Verify third number with first and second number to find out Larger and display if condition satisfy
.....................................................................*/
package week1.day1;

import java.util.Scanner;

public class GreatestOfThreeNum {

	public static void main(String[] args) {
		// Greatest of three number
		//read three float input from user
			System.out.println("Enter the Number1:" );
			Scanner scan=new Scanner(System.in);
			float n1= scan.nextFloat();
			System.out.println("Enter the Number2:" );
			float n2= scan.nextFloat();
			System.out.println("Enter the Number3:" );
			float n3= scan.nextFloat();
			//verify n1 with n2 and n3 to find out n1 is Larger
			if((n1>n2)&&(n1>n3)) {
				
				System.out.println("Number1 is Larger:"+n1);
			}
			//verify n2 with n1 and n3 to find out n2 is Larger
			else if((n2>n1)&&(n2>n3))
				{
					System.out.println("Number2 is Larger:"+n2);
			
			}
			//verify n3 with n2 and n1 to find out n3 is Larger
				else if((n3>n1&&n3>n2)) {
					System.out.println("Number3 is Larger:"+n3);
				}
				else
				{
					System.out.println("Cant find Larger number");
				}
				
			scan.close();
		}


	}

