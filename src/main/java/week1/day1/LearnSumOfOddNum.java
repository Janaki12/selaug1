package week1.day1;

import java.util.Scanner;

public class LearnSumOfOddNum {
	public static void main(String[] args) {
		// Get input from user and print sum of odd numbers in that.example:78253
		//get input from user
		Scanner S=new Scanner(System.in);
		int n=S.nextInt();
		//read each number one by one and sum it
		String s=Integer.toString(n);
		char[] c=s.toCharArray();
		int count=0;
		for(char o:c)
			if(o%2!=0) {
				count=count+o;
				System.out.println(count);	
				
			}  
	
			S.close();
}
	}

