package week1.day1;


public class LearnOddNumber {

	public static void main(String[] args) {
		// find odd number between 50 to 100
		int sum = 0;
		for (int i = 50; i <= 100; i++) 
		{
			if (i % 2 != 0) 
			{
				sum = sum + i;
			}
		}
		System.out.println("The Sum Of 50 to 100 Odd Numbers are:" + sum);
	}

	}


