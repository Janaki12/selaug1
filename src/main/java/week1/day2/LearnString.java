package week1.day2;

import java.util.Scanner;

public class LearnString {
	public static void main(String[] args) {
		// Amazon India Private Limited as input and find number of occurance of 'a' in this
		//get input from user
		Scanner S=new Scanner(System.in);
		//here we use nextLine to read space also,if we use nextInt it can read only first word before gap
		String n=S.nextLine();
		//convert the input string into all lowercase
		String a;
		a=n.toLowerCase();
	    System.out.println(a);
	    //to read single word one by one we convert to character array
		char[] c=a.toCharArray();
		//to read line by line  
		int count=0;
		//we use for each loop because we dont know starting and ending values
		for(char eachChar:c)
			//if expected result found loop will excute
		{
		if(eachChar=='a')
			count=count+1;
	}
		System.out.println(count);
		S.close();
	}
}

                                                    